from PyQt5 import QtWidgets, uic, QtGui
from PyQt5.QtCore import QObject, QThread, pyqtSignal
from PyQt5.QtWidgets import QMessageBox
import sys
from PyQt5.QtWidgets import *
import time
import threading
import json
from datetime import datetime
import csv
import pandas as pd


class Ui(QtWidgets.QMainWindow):
    def __init__(self):
        super(Ui, self).__init__()  # Call the inherited classes __init__ method
        uic.loadUi('dev.ui', self)  # Load the .ui file
        self.show()  # Show the GUI
        self.getdate()  # get data /time for field
        self.loadcsv()
        # rows  =  self.logbook.rowCount()
        # Connect button
        self.savebtn.clicked.connect(self.logcontact)
        # buttons for debugging
        self.savecsv.clicked.connect(self.savefile)
        self.loadbtn.clicked.connect(self.loadcsv)

    # load csv file into table
    def loadcsv(self):
        self.all_data = pd.read_csv('test2.csv')
        numRows = len(self.all_data.index)
        self.logbook.setColumnCount(len(self.all_data.columns))
        self.logbook.setRowCount(numRows)
        # self.logbook.setHorizontalHeaderLabels(self.all_data.columns)
        for i in range(numRows):
            for j in range(len(self.all_data.columns)):
                self.logbook.setItem(i, j, QTableWidgetItem(str(self.all_data.iat[i, j])))

    def savefile(self):
        handle = self.handle.text()
        handle = str(handle)
        print(handle)
        now = datetime.now()
        now = str(now)
        numbers = self.numbers.text()
        numbers = str(numbers)
        location = self.location.text()
        location = str(location)
        channel = self.channel.text()
        channel = str(channel)
        mode = self.mode.currentText()
        mode = str(mode)
        dx_contacts = {
            'Date': now,
            'Handle': [handle],
            'Numbers': [numbers],
            'Location': [location],
            'Channel': [channel],
            'Mode': [mode]
        }

        df = pd.DataFrame(dx_contacts)
        df.to_csv('test2.csv', index=False)

    # Save to CSV file    
    def savefileold(self):
        with open('test2.csv', "w") as fileOutput:
            writer = csv.writer(fileOutput)
            for rowNumber in range(self.logbook.rowCount()):
                fields = [
                    self.logbook.item(rowNumber, columnNumber).text() \
                        if self.logbook.item(rowNumber, columnNumber) is not None else ""
                    for columnNumber in range(self.logbook.columnCount())
                ]
                writer.writerow(fields)

    def getdate(self):
        self.now = datetime.now()
        self.dt.setDateTime(self.now)

    def clearfields(self):
        self.handle.setText("")
        self.numbers.setText("")
        self.location.setText("")
        self.channel.clear()
        self.notice.setText("")

    def logcontact(self):
        global mode
        global handle
        rows = self.logbook.rowCount()
        self.logbook.setRowCount(rows + 1)
        listi = []
        for i in range(rows):
            it = self.logbook.item(i, 0)
            if it and it.text():
                listi.append(it.text())
        if rows >= 1:
            rows = rows
        else:
            rows = rows
        handle = self.handle.text()
        handle = str(handle)
        now = datetime.now()
        now = str(now)
        numbers = self.numbers.text()
        numbers = str(numbers)
        location = self.location.text()
        location = str(location)
        channel = self.channel.text()
        channel = str(channel)
        mode = self.mode.currentText()
        print(mode)
        # write data to table
        self.logbook.setItem(rows, 0, QTableWidgetItem(now))
        self.logbook.setItem(rows, 1, QTableWidgetItem(handle))
        self.logbook.setItem(rows, 2, QTableWidgetItem(numbers))
        self.logbook.setItem(rows, 3, QTableWidgetItem(location))
        self.logbook.setItem(rows, 4, QTableWidgetItem(channel))
        self.logbook.setItem(rows, 5, QTableWidgetItem(mode))
        msg = QMessageBox()
        msg.setWindowTitle("PyDx")
        msg.setText("contact was saved")
        x = msg.exec_()
        self.savefile()
        self.clearfields()


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = Ui()
    app.exec_()
